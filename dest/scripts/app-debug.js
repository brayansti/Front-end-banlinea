'use strict';

var app = angular.module('banlinea-front', [
	'ui.router'
])


app.config(function($stateProvider, $urlRouterProvider){

	$stateProvider
		.state('signup', {
			url: '/signup',
			templateUrl: 'views/home.html',
			controller: 'HomeController',
			controllerAs: 'home'
		})
		.state('home',{
			url: '/home',
			templateUrl: 'views/signup.html',
			controller: 'SignupController',
			controllerAs: 'signup'
		});

	$urlRouterProvider.otherwise('/home');
	
});