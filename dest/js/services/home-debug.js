'use strict';

app.factory('HomeFactory', function($http){

	var url = 'https://contactmanager.banlinea.com/api/countries';
	var path = 'https://contactmanager.banlinea.com/api/ContactRegister';

	var HomeFactory = {

		getUsers: function(){
			return $http.get(url).success(function(data){
				return data;
			})
			.error(function(err){
				console.log(err);
			})
		},

		createUser: function(user){

			return $http.post(path, user).success(function(data){
				return data;
			})
			.error(function(err){
				console.log(err)
			})


		}

	};

	return HomeFactory

});