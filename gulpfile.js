'use strict';

var gulp = require('gulp');
var server = require('gulp-server-livereload');
 
var gulp = require('gulp');
var sass = require('gulp-sass');

var minify = require('gulp-minify');


// SERVER ↓↓
gulp.task('webserver', function() {
  gulp.src('dest/')
	.pipe(server({
	  livereload: true,
	  directoryListing: false,
	  open: true,
	  port: 8888
	}));
});


// SASS ↓↓
gulp.task('sass', function () {
  return gulp.src('src/sass/*.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(gulp.dest('dest/css'));
});
 


// JS Minificar ↓↓ 
gulp.task('compress', function() {
  gulp.src('src/scripts/**/*.js')
	.pipe(minify({
		ext:{
			src:'-debug.js',
			min:'.js'
		},
		exclude: ['tasks'],
		ignoreFiles: ['.combo.js', '-min.js']
	}))
	.pipe(gulp.dest('dest/scripts'))
});

// ↓↓ Tareas Watch
gulp.task('watch', function() {
  gulp.watch('src/scripts/**/*', ['compress']);
  gulp.watch('src/sass/*', ['sass'])
});

// ↓↓ Tareas Default
gulp.task('default', ['webserver', 'sass', 'compress' , 'watch']);