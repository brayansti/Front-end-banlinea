# Prueba-FrontEnd-Banlinea
Prueba para cargo de Front-End en Banlinea

## Instrucciones del aplicativo:

El aplicativo lo desarrollé sobre un entorno servidor basado en gulp, mediante el cual compilé hojas de estilo CSS y los archivos JS.

A continuación explico el procedimiento para ejecutar el aplicativo correctamente:

- Es necesario tener instalado nodeJs
- También se necesita instalar Gulp globalmente
- Las dependecias utilizadas son las siguientes:
```
gulp
gulp-server-livereload
gulp
gulp-sass
gulp-minify
```
- El archivo package.json ya está configurado para descargar todo lo necesario ejecutando:
```
npm install
```

- Luego de descargar las dependencias solamente ejecutar desde consola
```
gulp
```
- Este ejecutara la aplicación en el servidor  [http://localhost:8080/](http://localhost:8888/)


- Si hay algún problema con mucho gusto los ayudare.

Eso es todo, les agradezco la oportunidad que me están brindando,  y independiente del resultado de la prueba me gustaría tener una retroalimentación por parte de ustedes.
