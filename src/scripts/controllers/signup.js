'use strict';

app.controller('SignupController' , function(HomeFactory){
	var vm = this;

	vm.user = {};


	vm.createUser = function(user){
		// ↓↓ Datos del formulario
		console.dir(user);

		var jsonPretty = JSON.stringify(user, null, 4);

		console.log(jsonPretty);

		HomeFactory.createUser(user).then(function(data){
			console.log(data.data);
			alert('Usuario Registrado');
		});
	}
	

	vm.countries = {};

	HomeFactory.getCountries().then(function (paises) {

		vm.countries = paises.data;

		console.log(vm.countries);
	});



})