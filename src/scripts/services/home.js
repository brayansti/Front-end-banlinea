'use strict';

app.factory('HomeFactory', function($http){

	var url = 'https://contactmanager.banlinea.com/api/ContactRegister/';
	var urlPaises = 'https://contactmanager.banlinea.com/api/countries/';

	var HomeFactory = {

		getCountries: function(){
			return $http.get(urlPaises).success(function(data){
				return data;
			})
			.error(function(err){
				console.log(err);
			})
		},

		createUser: function(user){

			return $http.post(url, user).success(function(data){
				return data;
			})
			.error(function(err){
				console.log(err);
			})

		}

		/*createUser: function(user){

			return $http.post(path, user).success(function(data){
				return data;
			})
			.error(function(err){
				console.log(err)
			})

		}*/

	};

	return HomeFactory

});